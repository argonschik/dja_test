# Setup the URLs and include login URLs for the browsable API.
from rest_framework import routers, serializers, viewsets
from .views import PatientList, PatientCardList
from django.conf.urls import url, include, re_path
from django.urls import path



# # Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'patient', PatientList)
router.register(r'patientcard', PatientCardList)

urlpatterns = router.urls

# urlpatterns = [
#     re_path(r'^', include(router.urls)),
# ]

# urlpatterns += [
#     # path('admin/', admin.site.urls),
#     # path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
#     path('users/', UserList.as_view()),
#     path('users/<pk>/', UserDetails.as_view()),
#     path('groups/', GroupList.as_view()),
#     # ...
# ]
