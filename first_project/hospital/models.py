from django.db import models
from django.contrib.auth.models import User


class Patient(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Patient"

    def __str__(self):
        return self.user.name


class Doctor(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Doctor"

    def __str__(self):
        return self.user.name


class Cabinet(models.Model):
    scope = models.CharField(max_length=200)
    head = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)
    doctor = models.ManyToManyField(Doctor)

    class Meta:
        verbose_name = "Cabinet"

    def __str__(self):
        return self.scope


class Nurse(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    cabinet = models.ManyToManyField(Cabinet)

    class Meta:
        verbose_name = "Nurse"

    def __str__(self):
        return "{} {}".format(self.user.name, [i for i in self.cabinet.scope])


class PatientCard(models.Model):
    patient = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    patient_history = models.TextField()

    class Meta:
        verbose_name = "PatientCard"

    def __str__(self):
        return self.patient.name


