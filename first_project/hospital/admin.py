from django.contrib import admin
from .models import Cabinet, Doctor, Nurse, Patient, PatientCard
from django.contrib.auth.models import Permission


admin.site.register(Cabinet)
admin.site.register(Doctor)
admin.site.register(Nurse)
admin.site.register(Patient)
admin.site.register(PatientCard)




