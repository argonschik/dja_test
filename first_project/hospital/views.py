from django.shortcuts import render
from .models import Cabinet, PatientCard, Patient
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views import generic
from rest_framework import generics, permissions, serializers, viewsets
from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from rest_framework import generics, permissions, serializers
from .models import PatientCard, Patient
from rolepermissions.decorators import has_permission_decorator, has_role_decorator
from first_project.roles import DoctorRole, NurseRole, PatientRole, SystemAdminRole



class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields = '__all__'


class PatientCardSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatientCard
        fields = '__all__'


# @has_role_decorator('doctor')
@method_decorator(has_role_decorator(DoctorRole), name='dispatch')
class PatientList(viewsets.ModelViewSet):
    # class PatientList(generics.ListCreateAPIView):
    # permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]
    permission_classes = [permissions.AllowAny]
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer


class PatientCardList(viewsets.ModelViewSet):
    # class PatientCardList(generics.ListCreateAPIView):
    # permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]
    queryset = PatientCard.objects.all()
    serializer_class = PatientCardSerializer


# class UserDetails(generics.RetrieveAPIView):
#     permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]
#     queryset = User.objects.all()
#     serializer_class = UserSerializer
