from rolepermissions.roles import AbstractUserRole


class DoctorRole(AbstractUserRole):
    available_permissions = {
        'create_medical_record': True,
    }


class NurseRole(AbstractUserRole):
    available_permissions = {
        'edit_patient_file': True,
    }


class PatientRole(AbstractUserRole):
    available_permissions = {
        'read_patient_file': True,
    }


class SystemAdminRole(AbstractUserRole):
    available_permissions = {
        'drop_tables': True,
    }

# shell
# from rolepermissions.roles import assign_role, remove_role
# from django.contrib.auth.models import User
# user1 = User.objects.filter(name='user1')[0]
# from first_project.roles import DoctorRole
# assign_role(user1, DoctorRole)
# remove_role(user1, DoctorRole)

