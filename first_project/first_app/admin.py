from django.contrib import admin
from first_app.models import Post, Comment
from django.contrib.auth.models import Permission


admin.site.register(Permission)
admin.site.register(Post)
admin.site.register(Comment)

