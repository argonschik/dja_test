from django.db import models
from django.urls import reverse #Used to generate URLs by reversing the URL patterns


# Create your models here.

from django.template.defaultfilters import slugify
from django.contrib.auth.models import User


class Post(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(unique=True, max_length=255)
    content = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    author = models.TextField()

    class Meta:
        ordering = ['created_on']
        permissions = (("can_create", "Can create"),)

        def __unicode__(self):
            return self.title

    def __str__(self):
        return self.title

    # @models.permalink
    # def get_absolute_url(self):
    #     return ('blog_post_detail', (),
    #             {
    #                 'slug': self.slug,
    #             })

    def save(self, *args, **kwargs):
        super(Post, self).save(*args, **kwargs)
        # if not self.slug:
        #     self.slug = slugify(self.title)
        #     super(Post, self).save(*args, **kwargs)

    def get_absolute_url(self):
        """
        Returns the url to access a particular book instance.
        """
        return reverse('post-detail', args=[str(self.id)])


class Comment(models.Model):
    name = models.CharField(max_length=42)
    email = models.EmailField(max_length=75)
    website = models.URLField(max_length=200, null=True, blank=True)
    content = models.TextField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} - {}'.format(self.name, self.post)
