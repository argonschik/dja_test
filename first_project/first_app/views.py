from django.shortcuts import render
from .models import Post, Comment
from django.views import generic
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.core.exceptions import PermissionDenied
from oauth2_provider.views.generic import ProtectedResourceView
from django.http import HttpResponse




@login_required(login_url='/accounts/login/')
def index(request):
    num_posts = Post.objects.all().count()
    num_comments = Comment.objects.all().count()

    # Number of visits to this view, as counted in the session variable.
    num_visits=request.session.get('num_visits', 0)
    request.session['num_visits'] = num_visits+1

    return render(
        request,
        'index.html',
        context={'num_posts': num_posts, 'num_comments': num_comments, 'num_visits': num_visits}
    )


@method_decorator(login_required, name='dispatch')
class PostListView(generic.ListView):
    model = Post

    paginate_by = 3


@method_decorator(login_required, name='dispatch')
class PostDetailView(generic.DetailView):
    model = Post


class PostCreate(PermissionRequiredMixin, CreateView):
    model = Post
    fields = '__all__'
    initial={'slug':'tu-tu',}
    permission_required = ('first_app.add_post', )

    # def dispatch(self, request, *args, **kwargs):
    #     if not request.user.has_perm('first_app.add_post'):
    #         raise PermissionDenied
    #     return super(PostCreate, self).dispatch(request, *args, **kwargs)


# @method_decorator(login_required, name='dispatch')
# @method_decorator(permission_required('first_app.delete_post'), name='dispatch')
class PostDelete(PermissionRequiredMixin, DeleteView):
    model = Post
    success_url = reverse_lazy('posts')
    permission_required = ('first_app.delete_post', )

    # @method_decorator(login_required)
    # def dispatch(self, request, *args, **kwargs):
    #     if not request.user.has_perm('first_app.delete_post'):
    #         raise PermissionDenied
    #     return super(PostDelete, self).dispatch(request, *args, **kwargs)


class MyProtectedEndPoint(ProtectedResourceView):
    def get(self, request, *args, **kwargs):
        return HttpResponse('Hello, there!!')


