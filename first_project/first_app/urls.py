from django.urls import path, include, re_path
from .views import index, PostListView, PostDetailView, PostCreate, PostDelete, MyProtectedEndPoint

urlpatterns = [
    path('', index, name='index'),
    re_path(r'^posts/$', PostListView.as_view(), name='posts'),
    re_path(r'^post/(?P<pk>\d+)$', PostDetailView.as_view(), name='post-detail'),
    re_path(r'^post/create/$', PostCreate.as_view(), name='post_create'),
    # re_path(r'^post/(?P<pk>\d+)/update/$', PostUpdate.as_view(), name='post_update'),
    re_path(r'^post/(?P<pk>\d+)/delete/$', PostDelete.as_view(), name='post_delete'),
    re_path(r'^api/posts/protected_view/$', MyProtectedEndPoint.as_view(), name='my_protected_view'),
]



# GET ACCESS TOKEN
# curl -X POST -d "grant_type=password&username=username_value&password=password_value"
# -u"client_id_value:client_secret_value" http://localhost:8000/o/token/
#
# GET SERIALISED DATA
# curl -H "Authorization: Bearer access_token_value" http://localhost:8000/first_app/posts/

from rest_framework import generics, serializers
from .models import Post


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('title', )


class PostList1(generics.ListAPIView):
    # permission_classes = [permissions.IsAuthenticated, TokenHasScope]
    queryset = Post.objects.all()
    serializer_class = PostSerializer


urlpatterns += [
    path('posts_list/', PostList1.as_view()),
]
